from distutils.core import setup

setup(
    name='maapseq_algorithm',
    version='1.0',
    description='Run error collapse, MIRAGE, and pairSEQ on maapSEQ data.',
    author='Amy Ko',
    author_email='ako@adaptivebiotech.com',
    packages=['maapseq_algorithm'],
    install_requires=['numpy>=1.22.3', 'pandas>=1.4.2'],
    package_data={
        '': [
            'run_error_collapse.r', 
            'run_mirage.r',
            'run_pairseq.r',
            'post_pairing_error_collapse_v2.r'
        ]
},
    include_package_data=True
)
