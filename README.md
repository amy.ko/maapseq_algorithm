# About

maapseq_algorithm a Python package that runs error collapse, MIRAGE, and pairSEQ2 on maapSEQ data. It also contains utility functions that get alpha-beta-antigen hits from MIRAGE and pairSEQ2 outputs. 

**Warning**: The current version cannot handle unsorted pools. Please contact Amy Ko for questions. 



# Installation
## Install R dependencies
1) [sequenceCollapser](https://gitlab.com/adaptivebiotech/comp-bio/stat/tools/sequencecollapser): Standard error collapse algorithm
2) [**maapseq branch** of MIRAGE](https://gitlab.com/adaptivebiotech/comp-bio/stat/tools/mirage/-/tree/maapseq?ref_type=heads): A modified version of MIRAGE that takes in maapSEQ data to find antigen-specific betas or alphas
3) [pairsequel](https://gitlab.com/amy.ko/pairsequel): A modified version of pairSEQ2 that takes in maapSEQ data to find alpha-beta pairs

## Install maapseq_algorithm
Clone the repository, move into the directory where *setup.py* is, and type "pip install ."

    git clone git@gitlab.com:amy.ko/maapseq_algorithm.git
    cd maapseq_algorithm/
    pip install .



# How to run
## Make a config file
Make a text file that contains all the required parameters. Copy and paste the following into a text file and modify them for your experiment. Usually, the only parameters that need to be modified are those under [experiment] and *min_corr_occ* under [pairseq]. 

    [experiment]
    n_total_wells = 96 # total number of wells on the maapSEQ plate
    n_antigen_pools = 11 # e.g., in 11c6 design, this number is 11.
    n_nopep_pools = 1 # number of no-peptide pools
    n_pop_pools = 0 # number of "pool of pools"
    n_unsort_pools = 0 # number of unsort pools
    n_wells_per_pool = 8 # number of wells per letter pool
    n_wells_per_pool_np = 8 # number of wells per no-peptide pool
    address_length = 6 # e.g., in 11c6 design, this number is 6
    
    [pre_pairing_error_collapse]
    min_reads_per_seq_per_well = 2
    cdr3_dist_frac_pre_pairing = .035
    min_occ = 1
    
    [mirage]
    use_nopep = True
    use_unsort = False
    use_all_addresses = True
    eps = .01
    avg_num_cells_sorted_default = 1e8
    count_type = template
    
    [pairseq]
    min_corr_occ = 40 # minimum occupancy required for correlation pairing. Usually set to int(.8 * n_antigen_pools * n_wells_per_pool)
    min_occ_per_address = 4
    n_reps = 1000
    n_cores = 50
    min_bin_size = 20
    seq_freq_thresh = .2
    seed = 10
    ntier = 3
    prop_max_occ = .9
    
    [post_pairing_error_collapse]
    alpha_cdr3_threshold = .08
    beta_cdr3_threshold = .08
    qval_threshold = .001

## Initialize maapseq_algorithm object

    import maapseq_algorithm.Maapseq as Maapseq
    
    ms = Maapseq.Maapseq(config_path)

Here, *config_path* is the path to the text file containing the parameters from above. 

## Run sequenceCollapser

    ms.run_error_collapse(beta_tsv_paths, beta_error_collapse_output_dir)
    ms.run_error_collapse(alpha_tsv_paths, alpha_error_collapse_output_dir)

*beta_tsv_paths* is a list of paths to beta results.tsv files. The output from error collapse will be saved in the directory specified by *beta_error_collapse_output_dir*. Similar for alpha sequences. 


## Run MIRAGE
Run MIRAGE on beta sequences. We can do the same for alpha sequences but they're not used in finding paired maapSEQ hits. 

    ms.run_mirage(
        beta_error_collapse_output_dir, # directory containing error collapse results for beta
        beta_tsv_paths, # list of paths to beta results.tsv files
        antigen_key_path, # path to antigen key; must contain "address" column
        mirage_output_dir, # output directory for mirage results
        mirage_outfile_prefix, # prefix for output files
    )

## Run pairSEQ
Running this will create an output directory for each address. For example, the pairing results for address ABCDE will be in pairseq_output_dir/ABCDE/pairing_files/. 

    ms.run_pairseq(
        alpha_error_collapse_output_dir, # directory containing error collapse results for alpha
        beta_error_collapse_output_dir, # directory containing error collapse results for beta
        antigen_key_path, # path to antigen key; must contain "address" column
        pairseq_output_dir, # output directory for pairseq results
    ) 

## Get paired maapSEQ hits
The first three arguments are paths to MIRAGE equal prior results file, pairSEQ results directory, and error-collapsed alpha sequence info. The output is a dataframe that contains columns *sequence1*, *sequence2*, and *address*, which gives you alpha-beta-antigen hits. 

    import maapseq_algorithm.utils as msu
    import os
    
    paired_hits = msu.get_paired_hits(
        os.path.join(
            mirage_output_dir, mirage_outfile_prefix + "_post_prob_eql_prior.tsv"
        ), 
        pairseq_output_dir, 
        os.path.join(
            alpha_error_collapse_output_dir, "sequence_info_post_collapse_reads.txt"
        ), 
        mirage_qval=.05, 
        pairseq_qval=.001,
    )
