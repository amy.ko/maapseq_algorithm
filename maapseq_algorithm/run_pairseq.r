#!/usr/bin/Rscript --vanilla

## This script runs the pairSEQ2 model to identify light/heavy chain pairs.
## Error-cleaned processed sequence info files for both chains are required.

## Load required packages
library(pairsequel)
library(matrixStats)

## input arguments
tcrl_file_name <- ""        # [REQUIRED] path to the light-chain processed sequence info file
tcrh_file_name <- ""        # [REQUIRED] path to the heavy-chain processed sequence info file
tcrl_address_occ_path <- "" # [REQUIRED] path to occ per address tcra
tcrh_address_occ_path <- "" # [REQUIRED] path to occ per address for tcrb
address <- ""               # [REQUIRED] address string (e.g. 'ABCDEF')
pairing_path <- ""          # [REQUIRED] path to pairing results directory
plot_path <- ""             # [REQUIRED] path to plotting directory 
descriptive_path <- ""      # [REQUIRED] path to descriptive files directory
num_wells_per_pool <- 8     # <optional> number of wells per pool e.g. default is 8 in 96-well plate
min_occ_per_address <- 4    # <optional> min occupancy required for a sequence in the address to be processed (e.g. 4 out of 48 wells)
excluded_wells_path <- ""   # <optional> path to text file containing the wells to exclude such that each well is on a separate row
nreps <- 1000                # <optional> number of repetitions in the null simulation
num_cores <- 50             # <optional> number of cores to use for processing
min_corr_occ <- 40          # <optional> min occupancy threshold to utilize correlation signal
min_bin_size <- 20          # <optional> minimum number of TCRs required per occupancy bin
seq_freq_thresh <- 0.2      # <optional> if any clone frequency is above this threshold, plate normalization will not be run
num_wells_on_plate <- 96    # <optional> number of wells on the pairSEQ plate (only modify this parameter if you know what you are doing!)
seed <- 10                  # <optional> random seed
ntier <- 5                  # <optional> number of tiers
prop_max_occ <- .9          # <optional> filter out seq if it occurs < max_occ*prop_max_occ in this address

        
## -------------------------------------
## ---> INTERNAL UTILITY FUNCTIONS <----
## -------------------------------------

## function for parsing command line arguments: first two inputs are character
## vectors embedded in lists; third input is environment of the calling context
ProcessCommandArgs <- function(command.args, variables, parent.env) {
  variables <- unlist(variables)
  args <- strsplit(unlist(command.args), split='=')
  keys <- vector("character")
  values <- list()
  if (length(args) > 0) {
    for (i in 1:length(args)) {
      key <- args[[i]][1]
      value <- args[[i]][2]
      if (!(key %in% variables)) stop("\nUnrecognized option [",key,"].\n\n")
      if (grepl("\\)$", value) || grepl(":", value)) {
        internal.value <- eval(parse(text = value))
      }
      else {
        internal.value <- as.vector(value, mode=storage.mode(get(key, parent.env)))
      }
      keys <- c(keys, key)
      values <- c(values, list(internal.value))
    }
  }
  return(list("keys"=keys, "values"=values))
}


## -------------------------------------
## ----------> MAIN FUNCTION <----------
## -------------------------------------

## process command-line arguments
command.args <- ProcessCommandArgs(list(commandArgs(TRUE)), list(ls()), globalenv())
keys <- command.args$keys
if (length(keys) > 0) {
  for (i in 1:length(keys)) { assign(keys[i], command.args$values[[i]]) }
}

## check for existence of required inputs
stopifnot( file.exists(tcrl_file_name) )
stopifnot( file.exists(tcrh_file_name) )
stopifnot( dir.exists(pairing_path) )
stopifnot( dir.exists(plot_path) )

# Call run_pairSEQ2 function
run_pairsequel(tcrl_file_name = tcrl_file_name, 
             tcrh_file_name = tcrh_file_name, 
             tcrl_address_occ_path = tcrl_address_occ_path,
             tcrh_address_occ_path = tcrh_address_occ_path,               
             address = address, 
             top_seq_per_thr = seq_freq_thresh,
             pairing_path = pairing_path,
             plot_path = plot_path,
             descriptive_path = descriptive_path,
             num_wells_per_pool = num_wells_per_pool,
             nreps = nreps, 
             num_cores = num_cores,
             min_corr_occ = min_corr_occ, 
             min_bin_size = min_bin_size,
             num_wells_on_plate = num_wells_on_plate,
             excluded_wells_path = excluded_wells_path,
             seed = seed, 
             ntier = ntier,
             prop_max_occ = prop_max_occ,
             min_occ_per_address = min_occ_per_address)
