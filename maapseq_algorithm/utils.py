import pandas as pd
import os


def get_paired_hits(
    mirage_result_path,
    pairseq_root_dir,
    alpha_seqinfo_path,
    mirage_qval,
    pairseq_qval,
):
    """Get paired maapSEQ hits.
    Args:
        mirage_result_path (str): path to MIRAGE eql prior results
        paiseq_root_dir (str): directory containing pairseq results for all addresses
        alpha_seqinfo_path (str): path to alpha seq info file used in pairSEQ
        mirage_qval (float): qval threshold for calling MIRAGE hits
        pairseq_qval (float): qval threshold for calling pairSEQ hits
    Returns:
        paired_hits (df): Dataframe containing alpha-beta-antien hits (see columns sequence1, sequence2, address).
    """
    mirage_hits = get_mirage_hits(mirage_result_path, mirage_qval)
    pairs = get_pairs(pairseq_root_dir, pairseq_qval, "final")
    alpha_seqinfo = pd.read_csv(alpha_seqinfo_path, sep="\t")

    paired_hits = mirage_hits.merge(
        pairs,
        left_on=["nuc_seq", "top_specific_address"],
        right_on=["sequence2", "address"],
        suffixes=["_mirage", "_pairseq"],
    ).merge(
        alpha_seqinfo[["nucleotide", "sequenceStatus"]],
        left_on="sequence1",
        right_on="nucleotide",
    )
    # alpha has to productive
    paired_hits = paired_hits[paired_hits["sequenceStatus"] == "In"]
    paired_hits = paired_hits.drop(columns=["nucleotide", "sequenceStatus"])
    paired_hits = paired_hits.reset_index(drop=True)

    return paired_hits


def get_mirage_hits(mirage_result_path, qval):
    """Get MIRAGE hits. Hit if 1) below FDR threshold, 2) has valid address, 3) is productive
    Args:
        mirage_result_path (str): path to MIRAGE eql prior results
        qval (float): qval threshold
    Returns:

    """
    mirage_res = pd.read_csv(mirage_result_path, sep="\t")
    hits = mirage_res[
        (mirage_res["min_FDR"] <= qval)
        & (mirage_res["top_specific_is_valid_address"])
        & ~(mirage_res["peptide_seq"].str.contains("*", regex=False))  # stop codon
        & (mirage_res["peptide_seq"] != ".")  # out of frame
    ]
    hits = hits.reset_index(drop=True)

    return hits


def get_pairs(pairseq_root_dir, qval, pair_type="final"):
    """Get pairSEQ pairs from all addresses
    Args:
        pairseq_root_dir (str): directory containing pairseq results for all addresses
        qval (float): qval threshold
        pair_type (str): raw or final
    Returns:
        all_pairs (df): dataframe containing final pairs for all addresses. New column "address" is added.
    """
    pairseq_dirs = [
        os.path.join(pairseq_root_dir, x) for x in os.listdir(pairseq_root_dir)
    ]

    dfs = []
    for address_dir in pairseq_dirs:
        pairs = pd.read_csv(
            os.path.join(
                address_dir, "pairing_files", "tcr_pairseq2_{}.pairs".format(pair_type)
            ),
            sep="\t",
        )
        pairs = pairs[pairs["qValue"] <= qval]
        pairs["address"] = address_dir.split("/")[-1]
        dfs.append(pairs.copy())

    all_pairs = pd.concat(dfs)
    all_pairs = all_pairs.reset_index(drop=True)

    return all_pairs
