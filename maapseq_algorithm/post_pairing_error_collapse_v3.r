library(sequenceCollapser)
library(dplyr)
library(purrr)
library(data.table)
library(igraph)
library(stringdist)
library(optparse)

option_list <- list(
  make_option("--final_pairs", type = "character", help = "Path to tcr_pairseq_final.pairs"),
  make_option("--alpha_seqinfo", type = "character", help = "Path to tcra_processed_sequence_info.txt"),
  make_option("--beta_seqinfo", type = "character", help = "Path to tcrb_processed_sequence_info.txt"),
  make_option("--output_pairs_file", type = "character", help = "Path of output pairs file"),
  make_option("--output_map_file", type = "character", help = "Path of output map file"),
  make_option("--alpha_CDR3_threshold", default=0.08, type = "double", help = "alpha cdr3_fraction threshold (default 0.08)"),
  make_option("--beta_CDR3_threshold", default=0.08, type = "double", help = "beta cdr3_fraction threshold (default 0.08)"),
  make_option("--qvalue_threshold", default=0.01, type = "double", help = "q-value threshold")
)
opt <- parse_args(OptionParser(option_list=option_list))
#print(opt)

# opt$final_pairs <- "~/temp/cdr3_fraction_alpha_testing_pairseq2/crosspairing_hybrid.pairs"
# opt$alpha_seqinfo <- "~/temp/cdr3_fraction_alpha_testing_pairseq2/tcra_processed_sequence_info.txt"
# opt$beta_seqinfo <- "~/temp/cdr3_fraction_alpha_testing_pairseq2/tcrb_processed_sequence_info.txt"
# opt$alpha_CDR3_threshold <- 0.08
# opt$beta_CDR3_threshold <- 0.08
# opt$qvalue_threshold <- 0.01
# opt$output_pairs_file <- "~/temp/cdr3_fraction_alpha_testing_pairseq2/pairs_post_pairing_selection.pairs"
# opt$output_map_file <- "~/temp/cdr3_fraction_alpha_testing_pairseq2/pairs_post_pairing_selection.map"

fix_orphon_mismatches <- function(countmat) {
  if(is.character(countmat$vMismatches)) {
    countmat$vMismatches <- ifelse(countmat$vMismatches == ".", 999, as.numeric(countmat$vMismatches))
  }
  if(is.character(countmat$jMismatches)) {
    countmat$jMismatches <- ifelse(countmat$jMismatches == ".", 999, as.numeric(countmat$jMismatches))
  }
  return(countmat)
}


# Cluster sequences by CDR3 fraction and matching V or J genes
# Clustering is based on graph linkage
cluster_sequences <- function(df, cdr3_fraction = 0.08) {
  if(nrow(df) == 1) {
    df$cluster <- 1L
    return(df)
  }
  dmat <- stringdist::stringdistmatrix(df$cdr3, df$cdr3, method = "lv")
  cdr3Lens <- nchar(df$cdr3)
  for(i in 1:ncol(dmat)) {
    cdr3_threshold <- cdr3_fraction * cdr3Lens[i]
    x <- dmat[,i] <= cdr3_threshold
    x <- sapply(1:length(x), function(j) {
      if(x[j]) {
        return( areAnnotationsCompatible(df$vAnnotation[i], df$vAnnotation[j]) || areAnnotationsCompatible(df$jAnnotation[i], df$jAnnotation[j]) )
      } else {
        return ( FALSE )
      }
    })
    dmat[,i] <- x
  }
  gr <- graph_from_adjacency_matrix(dmat, mode = "undirected", diag = T)
  clust <- clusters(gr)
  df$cluster <- clust$membership
  return(df)
}

select_sequences <- function(df) {
  # If there is only 1 sequence just return it
  if(nrow(df) == 1) {
    df$clusterRep <- TRUE
    df$selected <- TRUE
    return(list(results = df, stats = NULL))
  }
  
  # If there is more than 1 sequence, group by cluster
  # Then select ONE representative sequence from each cluster:
  # * The top two sequences (by templates) are considered
  #   * In the extreme unlikely case where you have 3+ sequences with the same template count, break ties by vMismatches, jMismatches, then qValue, then nucleotide (alphabetical)
  # * Calculate "well ratio" metric
  #   * If well ratio and template counts disagree
  #   * Break ties by vMismatches, then by jMismatches
  #   * If still tied, default to template counts, then q-value then by nucleotide (alphabetical)
  res <- list()
  stats <- list()
  cluster_split <- split(df, df$cluster)
  for(i in 1:length(cluster_split)) {
    x <- cluster_split[[i]]
    if(nrow(x) == 1) {
      x$clusterRep <- TRUE
      res[[i]] <- x
      stats[[i]] <- data.frame(cluster = x$cluster[1], clusterqValue = min(x$qval), clusterRepTotalTemplates = x$totalTemplates[x$clusterRep], 
                               clusterRepvMismatches = x$vMismatches[x$clusterRep], clusterRepjMismatches = x$jMismatches[x$clusterRep],
                               clusterRepSequence = x$nucleotide[x$clusterRep])
      next;
    }
    x <- dplyr::arrange(x, desc(totalTemplates), vMismatches, jMismatches, qval, nucleotide) %>% dplyr::slice(1:2)
    
    ow1 <- x$occupiedWells[1] %>% strsplit(",") %>% unlist %>% as.numeric
    ow2 <- x$occupiedWells[2] %>% strsplit(",") %>% unlist %>% as.numeric
    
    tc1 <- x$templateCounts[1] %>% strsplit(",") %>% unlist %>% as.numeric
    tc2 <- x$templateCounts[2] %>% strsplit(",") %>% unlist %>% as.numeric
    nwells <- max(ow1, ow2)
    ot1 <- rep(0, nwells)
    ot1[ow1] <- tc1
    ot2 <- rep(0, nwells)
    ot2[ow2] <- tc2
    g1 <- sum(ot1 > ot2)
    g2 <- sum(ot2 > ot1)
    if(g2 > g1) {
      x <- dplyr::arrange(x, vMismatches, jMismatches, desc(totalTemplates), qval, nucleotide) %>% dplyr::slice(1)
    } else {
      x <- dplyr::slice(x, 1)
    }
    y <- cluster_split[[i]]
    y$clusterRep <- y$nucleotide == x$nucleotide
    res[[i]] <- y
    stats[[i]] <- data.frame(cluster = x$cluster[1], clusterqValue = min(y$qval), clusterRepTotalTemplates = y$totalTemplates[y$clusterRep], 
                             clusterRepvMismatches = y$vMismatches[y$clusterRep], clusterRepjMismatches = y$jMismatches[y$clusterRep],
                             clusterRepSequence = y$nucleotide[y$clusterRep])
  }
  
  # If there is more than one cluster
  # Select two clusters by best qValue, then template count (of representative)
  # If there are still ties, select by vMismatches, jMismatches, then nucleotide (alphabetical)
  
  stats <- data.table::rbindlist(stats)
  stats <- dplyr::arrange(stats, clusterqValue, desc(clusterRepTotalTemplates), clusterRepvMismatches, clusterRepjMismatches, clusterRepSequence)
  
  selected <- head(stats$clusterRepSequence, 2)
  res <- data.table::rbindlist(res)
  res$selected <- res$nucleotide %in% selected
  
  return(list(results = res, stats = stats))
}

final_pairs_cols <- c(sequence1 = "character", 
                      sequence2 = "character", 
                      occ1 = "numeric", 
                      occ2 = "numeric", 
                      occ1and2 = "numeric", 
                      jaccard = "numeric", 
                      occ_pval = "numeric", 
                      corr = "numeric", 
                      qval = "numeric"
                     )

seqinfo_cols <- c(nucleotide = "character", 
                  occupiedWells = "character", 
                  templateCounts = "character", 
                  vAnnotation = "character", 
                  jAnnotation = "character", 
                  vMismatches = "numeric" ,
                  jMismatches = "numeric", 
                  cdr3StartPosition = "numeric", 
                  cdr3Length = "numeric"
                 )

df0 <- fread(opt$final_pairs, colClasses = "character", nThread=1, data.table=F)
df <- fread(opt$final_pairs, colClasses = final_pairs_cols, nThread=1, data.table=FALSE)
countmat <- fread(opt$alpha_seqinfo, colClasses = seqinfo_cols, nThread=1, data.table=FALSE) %>% fix_orphon_mismatches
countmat <- countmat %>% mutate(cdr3 = substr(nucleotide, cdr3StartPosition, cdr3StartPosition + cdr3Length - 1))
countmat <- countmat %>% dplyr::select(-cdr3StartPosition, cdr3Length)
df <- df %>% dplyr::filter(qval <= opt$qvalue_threshold)
df <- dplyr::left_join(df, countmat, by = c("sequence1" = "nucleotide"))
df <- df %>% dplyr::mutate(meanTemplates = strsplit(templateCounts,",") %>% purrr::map_dbl(~ as.numeric(.x) %>% sum) %>% {./occ1})
df <- df %>% dplyr::mutate(totalTemplates = strsplit(templateCounts,",") %>% purrr::map_dbl(~ as.numeric(.x) %>% sum))

dflist <- split(df, df$sequence2)
stats <- list()
results <- list()
print("clustering TCRA sequences")
for(i in 1:length(dflist)) {
  if(i %% 1000 == 0) print(i)
  x <- cluster_sequences(dflist[[i]], cdr3_fraction = opt$alpha_CDR3_threshold)
  x$cluster <- paste0(i, ":", x$cluster)
  x$nucleotide <- x$sequence1 # temporary assignment
  res <- select_sequences(x)
  results[[i]] <- res$results
  stats[[i]] <- res$stats
}
res2 <- data.table::rbindlist(results) %>% as.data.frame
print(paste0("TCRA selected: ", sum(res2$selected)))

df2 <- res2 %>% dplyr::filter(selected)
df2 <- df2[,c(names(final_pairs_cols), "cluster", "clusterRep", "selected")]
df2 <- df2 %>% dplyr::rename(cluster1 = cluster, clusterRep1 = clusterRep, selected1 = selected)
countmat <- fread(opt$beta_seqinfo, colClasses = seqinfo_cols, nThread=1, data.table=FALSE) %>% fix_orphon_mismatches
countmat <- countmat %>% mutate(cdr3 = substr(nucleotide, cdr3StartPosition, cdr3StartPosition + cdr3Length - 1))
countmat <- countmat %>% dplyr::select(-cdr3StartPosition, cdr3Length)
df2 <- dplyr::left_join(df2, countmat, by = c("sequence2" = "nucleotide"))
df2 <- df2 %>% dplyr::mutate(meanTemplates = strsplit(templateCounts,",") %>% purrr::map_dbl(~ as.numeric(.x) %>% sum) %>% {./occ1})
df2 <- df2 %>% dplyr::mutate(totalTemplates = strsplit(templateCounts,",") %>% purrr::map_dbl(~ as.numeric(.x) %>% sum))
dflist <- split(df2, df2$sequence1)
stats <- list()
results <- list()
print("clustering TCRB sequences")
for(i in 1:length(dflist)) {
  if(i %% 1000 == 0) print(i)
  x <- cluster_sequences(dflist[[i]], cdr3_fraction = opt$beta_CDR3_threshold)
  x$cluster <- paste0(i, ":", x$cluster)
  x$nucleotide <- x$sequence2 # temporary assignment
  res <- select_sequences(x)
  results[[i]] <- res$results
  stats[[i]] <- res$stats
}
res3 <- data.table::rbindlist(results) %>% as.data.frame
res3 <- res3 %>% dplyr::rename(cluster2 = cluster, clusterRep2 = clusterRep, selected2 = selected)
res4 <- dplyr::filter(res3, selected2)
res4 <- dplyr::semi_join(df0, res4, by = c("sequence1", "sequence2"))
print(paste0("TCRB + TCRA selected: ", nrow(res4)))
fwrite(res4, file = opt$output_pairs_file, sep = "\t", quote=FALSE, na = "NA") # filtered final pairs

res5 <- df0 %>% dplyr::left_join(res2 %>% dplyr::transmute(sequence1, sequence2, cluster1=cluster, clusterRep1=clusterRep, selected1=selected), by = c("sequence1", "sequence2"))
res5 <- res5 %>% dplyr::left_join(res3 %>% dplyr::select(sequence1, sequence2, cluster2, clusterRep2, selected2), by = c("sequence1", "sequence2"))
res5$exclusion_reason <- with(res5, case_when(selected2 ~ ".",
                                   qval > opt$qvalue_threshold ~ "qvalue_threshold",
                                   !clusterRep1 ~ "alpha_error_derivative",
                                   !selected1 ~ "more_than_2_alpha_per_beta",
                                   !clusterRep2 ~ "beta_error_derivative",
                                   !selected2 ~ "more_than_2_beta_per_alpha"))
fwrite(res5, file = opt$output_map_file, sep = "\t", quote=FALSE)

# res5 %>% group_by(exclusion_reason) %>% summarize(n = n())
# ACTTCCTTCCACTTGAGGAAACCCTCAGTCCATATAAGCGACACGGCTGAGTACTTCTGTGCTGTGATATCGTACAGCAGTGCTTCCAAGATAATCTTTGGATCAGGGACCAGACTCAGCATCCGGCCAA

