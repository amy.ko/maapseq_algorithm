library(sequenceCollapser)
library(dplyr)
library(purrr)
library(data.table)

## Run error collapsing on immunoSEQ MIRA data. 
# Steps:
# 1. Run error collapse using read counts.
# 2. Make error collapsed sequence info files reads
# 3. Make error collapsed sequence info files for templates

## Input args
tsv_paths_str <- ""             # [REQUIRED] comma-delimited string of tsv paths
output_dir <- ""                # [REQUIRED] directory for output files
min_reads <- 1                  # <optional> require at least this many reads per seq per well 
cdr3_distance_fraction <- .035  # <optional> fraction of CDR3 bases that may differ between collapsed sequences
min_occ <- 3                    # <optional> filter out sequnces with occ lower than this

## -------------------------------------
## ---> INTERNAL UTILITY FUNCTIONS <----
## -------------------------------------

## function for parsing command line arguments: first two inputs are character
## vectors embedded in lists; third input is environment of the calling context
ProcessCommandArgs <- function(command.args, variables, parent.env) {
  variables <- unlist(variables)
  args <- strsplit(unlist(command.args), split='=')
  keys <- vector("character")
  values <- list()
  if (length(args) > 0) {
    for (i in 1:length(args)) {
      key <- args[[i]][1]
      value <- args[[i]][2]
      if (!(key %in% variables)) stop("\nUnrecognized option [",key,"].\n\n")
      if (grepl("\\)$", value) || grepl(":", value)) {
        internal.value <- eval(parse(text = value))
      }
      else {
        internal.value <- as.vector(value, mode=storage.mode(get(key, parent.env)))
      }
      keys <- c(keys, key)
      values <- c(values, list(internal.value))
    }
  }
  return(list("keys"=keys, "values"=values))
}

## process command-line arguments
command.args <- ProcessCommandArgs(list(commandArgs(TRUE)), list(ls()), globalenv())
keys <- command.args$keys
if (length(keys) > 0) {
  for (i in 1:length(keys)) { assign(keys[i], command.args$values[[i]]) }
}

get_unique_sequences_per_well <- function(
    tsv_data, template_id, min_reads, productive_only
) {
  sapply(tsv_data, function(x) {
    if(nrow(x) == 0) return(0)
    s <- as.integer(x[[template_id]]) >= min_reads
    if(productive_only) s <- s & x$sequenceStatus == "In"
    sum(s)
  })
}

## -------------------------------------
## ----------> MAIN FUNCTION <----------
## -------------------------------------

################## Parse TSV data ########################
# get tsv files
tsv_files <- strsplit(tsv_paths_str, ',')[[1]]
n_total_wells <- length(tsv_files)

# read in data; copy is always included by default
data <- readInData(
    results_tsv_files_array=tsv_files, 
    template_id="inputTemplateEstimate", 
    nthreads = 1
)
# mark templates that don't meet the min_reads requirement
for (i in 1:n_total_wells){
    data[[i]] <- data[[i]] %>% 
    mutate(inputTemplateEstimate = ifelse(as.integer(copy)>=min_reads,
                                   inputTemplateEstimate, '-1'))
}

# SKIP: exclude wells with too few or too many seqs

# parse tsv files into seqinfo
seqinfo <- sequenceCollapser::parseResultsTsvFiles(
    df=data, 
    germline=data.frame(), 
    count_label='copy', 
    excluded_wells=integer(), 
    min_reads=min_reads, 
    productive_only=FALSE, 
    count_germline_mismatches=FALSE
)
seqinfo$mean_templates <- 
    seqinfo$templateCounts %>% 
    strsplit(",") %>% 
    purrr::map_dbl(~as.numeric(.x) %>% mean)
seqinfo$n_wells <- 
    seqinfo$occupiedWells %>% 
    stringr::str_count(",") %>% 
    {. + 1}
seqinfo <- dplyr::arrange(
    seqinfo, 
    desc(n_wells), 
    desc(mean_templates), 
    nucleotide
)
seqinfo_pre_collapse <- dplyr::filter(seqinfo, n_wells >= min_occ)


# if paired-end, merge reads to get consensus sequences
#if (seq_type == 'paired'){
#    post_read_merge <- readMerge(seqinfo, n_total_wells)
#    seqinfo_pre_collapse <- post_read_merge$merge_seqinfo
#    read_merge_map <- post_read_merge$merge_events %>% 
#    rename(parent=merge_parent, child=merge_child)
#} else {
#    seqinfo_pre_collapse <- seqinfo
#}
#seqinfo_pre_collapse <- dplyr::filter(seqinfo_pre_collapse, n_wells >= min_occ)


##################### Error collapse ######################
# collapse seqs
collapsed <- printFilteredSequenceInfo(
    seqinfo_pre_collapse, 
    cdr3_distance_fraction = cdr3_distance_fraction, 
    return_child_mapping = TRUE
)
seqinfo_post_collapse <- collapsed[[1]] # this just removes error derivative rows
error_collapse_map <-collapsed[[2]]
# merge counts
seqinfo_post_collapse_merged <- errorMerge( 
    collapsed_seqinfo = seqinfo_post_collapse, 
    precollapsed_seqinfo = seqinfo_pre_collapse, 
    collapse_map = error_collapse_map
)

################### Seqinfo for templates #####################
seqinfo_template <- sequenceCollapser::parseResultsTsvFiles(
    df=data, 
    germline=data.frame(), 
    count_label='inputTemplateEstimate', 
    excluded_wells=integer(), 
    min_reads=0, 
    productive_only=FALSE, 
    count_germline_mismatches=FALSE
)
seqinfo_template$mean_templates <- 
    seqinfo_template$templateCounts %>% 
    strsplit(",") %>% 
    purrr::map_dbl(~as.numeric(.x) %>% mean)
seqinfo_template$n_wells <- 
    seqinfo_template$occupiedWells %>% 
    stringr::str_count(",") %>% 
    {. + 1}
seqinfo_template <- dplyr::arrange(
    seqinfo_template, 
    desc(n_wells), 
    desc(mean_templates), 
    nucleotide
)
seqinfo_pre_collapse_template <- dplyr::filter(seqinfo_template, n_wells >= min_occ)

## collapse seqinfo templates
seqinfo_post_collapse_template <- 
    seqinfo_pre_collapse_template %>%
    dplyr::left_join(error_collapse_map, by = c("nucleotide" = "child")) %>%
    dplyr::filter(is.na(parent)) %>%
    dplyr::select(-parent)

seqinfo_post_collapse_merged_template <- errorMerge( # merge counts
    collapsed_seqinfo = seqinfo_post_collapse_template, 
    precollapsed_seqinfo = seqinfo_pre_collapse_template, 
    collapse_map = error_collapse_map
)


#################### Descriptive files #####################
# unique seqs per well (original data, meets min_reads requirement)
unique_sequences_per_well <- get_unique_sequences_per_well(
    tsv_data=data, 
    template_id='copy', 
    min_reads=min_reads, 
    productive_only=FALSE
)
seqs_per_well <- data.frame(
    well=1:n_total_wells, 
    count=unique_sequences_per_well, 
    stringsAsFactors=FALSE
)
# unique seqs per occ (meets min_reads, read merged if paired)
occ_counts <- stringr::str_count(seqinfo_pre_collapse$occupiedWells,",") + 1
unique_sequences_per_occ <- sapply(
    1:n_total_wells, function(i) sum(occ_counts == i)
)
seqs_per_occ <- data.frame(
    well=1:n_total_wells, 
    count=unique_sequences_per_occ, 
    stringsAsFactors=FALSE
)
# unique seqs pre, post
seqs_pre_post <- data.frame(
    type=c('pre', 'post'), 
    count=c(dim(seqinfo_pre_collapse)[1], dim(seqinfo_post_collapse)[1]), 
    stringsAsFactors=FALSE
)
# well_num to name map
well_name_map <- data.frame(
    well_num = 1:length(tsv_files),
    well_name = tsv_files
)

################## Write to file ###################
data.table::fwrite(
    seqinfo_post_collapse_merged, 
    file = file.path(output_dir, 'sequence_info_post_collapse_reads.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_pre_collapse, 
    file = file.path(output_dir, 'sequence_info_pre_collapse_reads.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_post_collapse_merged_template, 
    file = file.path(output_dir, 'sequence_info_post_collapse_templates.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqinfo_pre_collapse_template, 
    file = file.path(output_dir, 'sequence_info_pre_collapse_templates.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    well_name_map, 
    file = file.path(output_dir, 'well_name.map'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    error_collapse_map, 
    file = file.path(output_dir, 'error_collapse.map'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqs_per_well, 
    file = file.path(output_dir, 'unique_sequences_per_well.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqs_per_occ, 
    file = file.path(output_dir, 'unique_sequences_per_occ.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)
data.table::fwrite(
    seqs_pre_post, 
    file = file.path(output_dir, 'unique_sequences_pre_post.txt'), 
    sep = "\t", 
    quote=FALSE, 
    col.names=TRUE
)

#if (seq_type == "paired"){
#    data.table::fwrite(
#        seqinfo, 
#        file = file.path(output_dir, "sequence_info_pre_read_merge_reads.txt"), 
#        sep = "\t", 
#        quote=FALSE, 
#        col.names=TRUE
#    )
#    data.table::fwrite(
#        read_merge_map, 
#        file = file.path(output_dir, "sequence_info_read_merge.map"), 
#        sep = "\t", 
#        quote=FALSE, 
#        col.names=TRUE
#    )
#}