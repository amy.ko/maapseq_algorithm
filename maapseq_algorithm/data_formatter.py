import pandas as pd
import numpy as np
from string import ascii_uppercase
import re


def get_gene_annotations(tsv_paths, target_nucleotides):
    """Get gene annotations for target nucleotides from results tsv files.
    Args:
        tsv_paths (list): list of results tsv paths
        target_nucleotides (list): list of nucleotides to get gene annotations for
    Output:
        annotations (df): dataframe containing gene annotations for each nucleotide
    """
    cols = [
        "nucleotide",
        "vFamilyName",
        "vGeneName",
        "vGeneAllele",
        "vFamilyTies",
        "vGeneNameTies",
        "vGeneAlleleTies",
        "dFamilyName",
        "dGeneName",
        "dGeneAllele",
        "dFamilyTies",
        "dGeneNameTies",
        "dGeneAlleleTies",
        "jFamilyName",
        "jGeneName",
        "jGeneAllele",
        "jFamilyTies",
        "jGeneNameTies",
        "jGeneAlleleTies",
    ]
    df_list = []
    for tsv in tsv_paths:
        df = pd.read_csv(tsv, sep="\t", comment="#", low_memory=False)
        df = df[df["nucleotide"].isin(target_nucleotides)][cols]
        df_list.append(df.copy())

    df = pd.concat(df_list, axis=0)
    df.drop_duplicates("nucleotide", inplace=True)
    return df


def make_mirage_count_table(
    seqinfo_path,
    tsv_paths,
    count_table_path,
    n_antigen_pools,
    n_nopep_pools,
    n_pop_pools,
    n_unsort_pools,
    n_total_wells,
    n_wells_per_pool,
    n_wells_per_pool_np,
):
    """Make count table for mirage.
    Args:
        seqinfo_path (str): path to error collapsed seq info file
        tsv_paths (list): list of results tsv paths
        count_table_path (str): path to count table output
        n_antigen_pools (int): number of antigen pools e.g. 11 for a 11c6 design
        n_nopep_pools (int): number of nopep pools
        n_pop_pools (int): number of PoP pools
        n_unsort_pools (int): number of unsort pools
        n_total_wells (int): number of total wells on plate
        n_wells_per_pool (int): number of wells for each letter pool
        n_wells_per_pool_np (int): number of wells for each no-pep pool
    Output:
        Save count table in count_table_path
    """
    # load seq info
    seq_info = pd.read_csv(
        seqinfo_path,
        sep="\t",
        dtype={"occupiedWells": str, "templateCounts": str},
    )
    n_seq = seq_info.shape[0]

    # populate counts for all wells
    count_table = np.zeros((n_seq, n_total_wells), int)
    for i, row in seq_info.iterrows():
        cols = [int(x) - 1 for x in row["occupiedWells"].split(",")]
        vals = [int(x) for x in row["templateCounts"].split(",")]
        count_table[i, cols] = vals

    # set pool names. Assume wells are ordered from A,B, ..., nopep, pop, unsort
    pool_names = []
    for i in range(n_antigen_pools):
        for j in range(n_wells_per_pool):
            pool_names.append("{}_positive_{}".format(ascii_uppercase[i], j + 1))
    for i in range(n_nopep_pools):
        for j in range(n_wells_per_pool_np):
            pool_names.append("nopep{}_{}".format(i + 1, j + 1))
    for i in range(n_pop_pools):
        for j in range(n_wells_per_pool):
            pool_names.append("pop{}_{}".format(i + 1, j + 1))
    for i in range(n_unsort_pools):
        for j in range(n_wells_per_pool):
            pool_names.append("unsorted_expanded{}_{}".format(i + 1, j + 1))

    # make count_df
    count_df = pd.DataFrame(count_table, columns=pool_names).astype(int)

    # combine with meta info
    count_df_final = pd.concat(
        [seq_info[["nucleotide", "aminoAcid"]], count_df],
        axis=1,
    )
    gene_annotations = get_gene_annotations(tsv_paths, seq_info["nucleotide"])
    count_df_final = count_df_final.merge(gene_annotations, how="left", on="nucleotide")

    # save
    count_df_final.to_csv(count_table_path, sep="\t", index=False)


def filter_seqinfo_to_address(seq_info_path, address, n_wells_per_pool=8):
    """Filter seqinfo to the given address.
    Args:
        seq_info_path (str): path to seqinfo
        address (str): e.g. ABCDEF
        n_wells_per_pool (int): num wells in each pool
    Output:
        Replace input file with filtered seqinfo
    """
    seq_info = pd.read_csv(seq_info_path, sep="\t")

    wells_to_keep = []
    for letter in address:
        i = ascii_uppercase.index(letter.upper())
        start = i * n_wells_per_pool
        end = (i + 1) * n_wells_per_pool
        wells_to_keep += [str(x + 1) for x in range(start, end)]
    wells_to_keep = set(wells_to_keep)

    seq_info_filtered = seq_info.copy()
    for i, row in seq_info.iterrows():
        wells = row["occupiedWells"].split(",")
        counts = row["templateCounts"].split(",")
        filtered_tuple = [
            (well, count) for well, count in zip(wells, counts) if well in wells_to_keep
        ]
        new_wells = [x[0] for x in filtered_tuple]
        new_counts = [x[1] for x in filtered_tuple]
        seq_info_filtered.loc[i, "occupiedWells"] = ",".join(new_wells)
        seq_info_filtered.loc[i, "templateCounts"] = ",".join(new_counts)
        seq_info_filtered.loc[i, "mean_templates"] = (
            np.array(new_counts, float).mean() if len(new_wells) > 0 else 0
        )
        seq_info_filtered.loc[i, "n_wells"] = len(new_wells)

    seq_info_filtered.to_csv(seq_info_path, sep="\t", index=False)


def get_address_cols(address, n_wells_per_pool=8):
    """Get the well indices for a given address. E.g. 'A' -> [0,1,2,3,4,5,6,7]
    Args:
        address (str): address
        num_wells_per_pool (int): num wells per pool
    Output:
        list of well indicies corresponding to the given address
    """
    cols = []
    for letter in address:
        i = ascii_uppercase.index(letter.upper())
        start = i * n_wells_per_pool
        end = (i + 1) * n_wells_per_pool
        cols += [x for x in range(start, end)]
    return cols


def make_occupancy_per_address(
    seqinfo_path, antigen_key_path, output_path, n_total_wells, n_wells_per_pool
):
    """For each sequence, record occupancy in each address.
    Args:
        seqinfo_path (str): path to seqinfo file
        antigen_key_path (str): path to antigen key
        output_path (str): output path
        n_total_wells (str): total number of wells on plate
        n_wells_per_pool (int): num wells per pool
    Output:
        Saves occ per address (nuc, occ_ABCDEF, occ_ABCDEG, ...) in output_path
    """
    # load seq info
    seq_info = pd.read_csv(
        seqinfo_path,
        sep="\t",
        dtype={"occupiedWells": str, "templateCounts": str},
    )
    n_seq = seq_info.shape[0]

    # populate counts in all wells
    count_table = np.zeros((n_seq, n_total_wells), int)
    for i, row in seq_info.iterrows():
        cols = [int(x) - 1 for x in row["occupiedWells"].split(",")]
        vals = [int(x) for x in row["templateCounts"].split(",")]
        count_table[i, cols] = vals

    # occ per address
    antigen_key = pd.read_csv(antigen_key_path, sep="\t")
    occ_per_address = pd.DataFrame(
        np.zeros((n_seq, len(antigen_key["address"])), int),
        columns=antigen_key["address"].values,
    )
    for address in antigen_key["address"]:
        cols = get_address_cols(address, n_wells_per_pool)
        occ = (count_table[:, cols] > 0).sum(axis=1)
        occ_per_address.loc[:, address] = occ

    # save file
    final_df = pd.concat([seq_info["nucleotide"], occ_per_address], axis=1)
    final_df.to_csv(output_path, sep="\t", index=False)
