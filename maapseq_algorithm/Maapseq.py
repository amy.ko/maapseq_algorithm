import configparser
import os
import pandas as pd
import subprocess
import maapseq_algorithm.data_formatter as data_formatter


class Maapseq(object):
    def __init__(self, config_path):
        self.config = configparser.ConfigParser()
        self.config.read(config_path)
        self.rscript_dir = os.path.dirname(__file__)

    def run_error_collapse(self, tsv_paths, output_dir):
        """Run pre-pairing error collapse
        Args:
            tsv_paths (list): list of tsv paths
            output_dir (str): output directory for error collapse files
        Output:
            Save sequence info files and other error collapse results to output_dir
        """
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        tsv_paths_str = ",".join(sorted(tsv_paths))
        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "run_error_collapse.r"),
            "tsv_paths_str=" + tsv_paths_str,
            "output_dir=" + output_dir,
            "min_reads="
            + self.config["pre_pairing_error_collapse"]["min_reads_per_seq_per_well"],
            "cdr3_distance_fraction="
            + self.config["pre_pairing_error_collapse"]["cdr3_dist_frac_pre_pairing"],
            "min_occ=" + self.config["pre_pairing_error_collapse"]["min_occ"],
        ]
        subprocess.run(command)

    def run_mirage(
        self,
        error_collapse_dir,
        tsv_paths,
        antigen_key_path,
        output_dir,
        outfile_prefix,
    ):
        """Run MIRAGE.
        Args:
            error_collapse_dir (str): directory containing error collapsed files
            tsv_paths (list): list of results tsv paths
            antigen_key_path (str): path to antigen key
            output_dir (str): directory to contain MIRAGE output files
            outfile_prefix (str): prefix for MIRAGE output files
        Output:
            Save MIRAGE input and output files to output_dir/mirage_prefix_*
        """
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # make count table
        count_type = (
            "reads"
            if "READ" in self.config["mirage"]["count_type"].upper()
            else "templates"
        )
        seqinfo_path = os.path.join(
            error_collapse_dir,
            "sequence_info_post_collapse_{count_type}.txt".format(
                count_type=count_type
            ),
        )
        count_table_path = os.path.join(output_dir, outfile_prefix + "_count_table.txt")
        data_formatter.make_mirage_count_table(
            seqinfo_path,
            tsv_paths,
            count_table_path,
            n_antigen_pools=int(self.config["experiment"]["n_antigen_pools"]),
            n_nopep_pools=int(self.config["experiment"]["n_nopep_pools"]),
            n_pop_pools=int(self.config["experiment"]["n_pop_pools"]),
            n_unsort_pools=int(self.config["experiment"]["n_unsort_pools"]),
            n_total_wells=int(self.config["experiment"]["n_total_wells"]),
            n_wells_per_pool=int(self.config["experiment"]["n_wells_per_pool"]),
            n_wells_per_pool_np=int(self.config["experiment"]["n_wells_per_pool_np"]),
        )

        use_nopep_r_str = (
            "TRUE" if self.config["mirage"]["use_nopep"].upper() == "TRUE" else "FALSE"
        )
        use_unsort_r_str = (
            "TRUE" if self.config["mirage"]["use_unsort"].upper() == "TRUE" else "FALSE"
        )
        use_all_addresses_r_str = (
            "TRUE"
            if self.config["mirage"]["use_all_addresses"].upper() == "TRUE"
            else "FALSE"
        )

        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "run_mirage.r"),
            "mira_count_table_path=" + count_table_path,
            "num_total_wells=" + self.config["experiment"]["n_antigen_pools"],
            "num_antigen_wells=" + self.config["experiment"]["address_length"],
            "num_wells_per_pool=" + self.config["experiment"]["n_wells_per_pool"],
            "num_wells_per_pool_np=" + self.config["experiment"]["n_wells_per_pool_np"],
            "output_path_prefix=" + os.path.join(output_dir, outfile_prefix),
            "key_path=" + antigen_key_path,
            "use_nopep=" + use_nopep_r_str,
            "use_unsort=" + use_unsort_r_str,
            "use_all_addresses=" + use_all_addresses_r_str,
            "eps=" + self.config["mirage"]["eps"],
            "avg_num_cells_sorted_default="
            + self.config["mirage"]["avg_num_cells_sorted_default"],
        ]
        subprocess.run(command)

    def run_pairseq(
        self,
        tcra_error_collapse_dir,
        tcrb_error_collapse_dir,
        antigen_key_path,
        output_dir,
    ):
        """Run pairseq for all addresses
        Args:
            tcra_error_collapse_dir (str): directory containing error collapse results for alpha
            tcrb_error_collapse_dir (str): directory containing error collapse results for beta
            antigen_key_path (str): path to antigen key; must contain "address" column
            output_dir (str): directory for pairseq output files
        Output:
            Save pairseq output files in output_dir
        """
        # make input files necessary for pairseq
        tcra_seq_info_path = os.path.join(
            tcra_error_collapse_dir, "sequence_info_post_collapse_reads.txt"
        )
        tcrb_seq_info_path = os.path.join(
            tcrb_error_collapse_dir, "sequence_info_post_collapse_reads.txt"
        )
        tcra_address_occ_path = os.path.join(
            tcra_error_collapse_dir, "occ_per_address.txt"
        )
        tcrb_address_occ_path = os.path.join(
            tcrb_error_collapse_dir, "occ_per_address.txt"
        )
        data_formatter.make_occupancy_per_address(
            tcra_seq_info_path,
            antigen_key_path,
            tcra_address_occ_path,
            int(self.config["experiment"]["n_total_wells"]),
            int(self.config["experiment"]["n_wells_per_pool"]),
        )
        data_formatter.make_occupancy_per_address(
            tcrb_seq_info_path,
            antigen_key_path,
            tcrb_address_occ_path,
            int(self.config["experiment"]["n_total_wells"]),
            int(self.config["experiment"]["n_wells_per_pool"]),
        )

        # run pairseq for each address
        addresses = pd.read_csv(antigen_key_path, sep="\t")["address"]
        for address in addresses:
            self.run_pairseq_given_address(
                tcra_seq_info_path,
                tcrb_seq_info_path,
                tcra_address_occ_path,
                tcrb_address_occ_path,
                output_dir,
                address,
            )

    def run_pairseq_given_address(
        self,
        tcra_seq_info_path,
        tcrb_seq_info_path,
        tcra_address_occ_path,
        tcrb_address_occ_path,
        output_dir,
        address,
    ):
        """Run pairseq for the given address
        Args:
            tcra_seq_info_path (str): path to tcra seqinfo file
            tcrb_seq_info_path (str): path to tcrb seqinfo file
            tcra_address_occ_path (str): path to tcra occ per address file
            tcrb_address_occ_path (str): path to tcrb occ per address file
            output_dir (str): directory for pairseq output files
            address (str): address e.g. 'ABCDEF'
        Output:
            Save pairseq output files in output_dir
        """
        descriptive_dir = os.path.join(output_dir, address, "descriptive_files/")
        figures_dir = os.path.join(output_dir, address, "figures/")
        pairing_dir = os.path.join(output_dir, address, "pairing_files/")
        if not os.path.exists(descriptive_dir):
            os.makedirs(descriptive_dir)
        if not os.path.exists(figures_dir):
            os.makedirs(figures_dir)
        if not os.path.exists(pairing_dir):
            os.makedirs(pairing_dir)

        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "run_pairseq.r"),
            "tcrl_file_name=" + tcra_seq_info_path,
            "tcrh_file_name=" + tcrb_seq_info_path,
            "tcrl_address_occ_path=" + tcra_address_occ_path,
            "tcrh_address_occ_path=" + tcrb_address_occ_path,
            "address=" + address,
            "pairing_path=" + pairing_dir,
            "plot_path=" + figures_dir,
            "descriptive_path=" + descriptive_dir,
            "num_wells_per_pool=" + self.config["experiment"]["n_wells_per_pool"],
            "nreps=" + self.config["pairseq"]["n_reps"],
            "num_cores=" + self.config["pairseq"]["n_cores"],
            "min_corr_occ=" + self.config["pairseq"]["min_corr_occ"],
            "min_bin_size=" + self.config["pairseq"]["min_bin_size"],
            "seq_freq_thresh=" + self.config["pairseq"]["seq_freq_thresh"],
            "num_wells_on_plate=" + self.config["experiment"]["n_total_wells"],
            "seed=" + self.config["pairseq"]["seed"],
            "ntier=" + self.config["pairseq"]["ntier"],
            "min_occ_per_address=" + self.config["pairseq"]["min_occ_per_address"],
            "prop_max_occ=" + self.config["pairseq"]["prop_max_occ"],
        ]
        subprocess.run(command)

        # make filtered seqinfo for this address
        tcra_filtered_seq_info_path = os.path.join(
            descriptive_dir, "tcra_sequence_info.txt"
        )
        tcrb_filtered_seq_info_path = os.path.join(
            descriptive_dir, "tcrb_sequence_info.txt"
        )
        n_wells_per_pool = int(self.config["experiment"]["n_wells_per_pool"])

        data_formatter.filter_seqinfo_to_address(
            tcra_filtered_seq_info_path, address, n_wells_per_pool
        )
        data_formatter.filter_seqinfo_to_address(
            tcrb_filtered_seq_info_path, address, n_wells_per_pool
        )

        # run post-pairing error collapse
        self.run_post_pairing_error_collapse(descriptive_dir, pairing_dir)

    def run_post_pairing_error_collapse(self, descriptive_dir, pairing_dir):
        """Run post-pairseq error collapse
        Args:
            descriptive_dir (str): directory containing filtered seq info files
            pairing_dir (str): directory containing raw pairs file
        Output:
            Save final.pairs to input_dir/pairing_files
        """
        command = [
            "Rscript",
            os.path.join(self.rscript_dir, "post_pairing_error_collapse_v2.r"),
            "--final_pairs",
            pairing_dir + "tcr_pairseq2_raw.pairs",
            "--alpha_seqinfo",
            descriptive_dir + "tcra_sequence_info.txt",
            "--beta_seqinfo",
            descriptive_dir + "tcrb_sequence_info.txt",
            "--output_pairs_file",
            pairing_dir + "tcr_pairseq2_final.pairs",
            "--output_map_file",
            pairing_dir + "tcr_pairseq2_final.map",
            "--alpha_CDR3_threshold",
            self.config["post_pairing_error_collapse"]["alpha_cdr3_threshold"],
            "--beta_CDR3_threshold",
            self.config["post_pairing_error_collapse"]["beta_cdr3_threshold"],
            "--qvalue_threshold",
            self.config["post_pairing_error_collapse"]["qval_threshold"],
        ]
        subprocess.run(command)
